from flask import Flask
from flask_login import LoginManager

from flask_mongoengine import MongoEngine
from app.config import Config

app = Flask(__name__)
app.config.from_object(Config)
db = MongoEngine(app)
login_manager = LoginManager(app)

from app.routes import *
