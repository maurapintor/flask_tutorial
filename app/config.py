import os


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or "xzkjvn8dddaojclkvnfjnveini3409"

    MONGODB_HOST = "localhost"
    MONGODB_PORT = 27017
    MONGODB_USERNAME = "maurapintor"
    MONGODB_CONNECT = True
    MONGODB_DB = "my_app_db"