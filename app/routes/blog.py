from flask import render_template
from flask_login import login_required, current_user

from app import app
from app.models import Comment


@app.route('/')
@app.route('/index')
def index():
    user = current_user
    comments = Comment.objects
    return render_template('blog.html',
                           title='Home',
                           user=user,
                           comments=comments)
