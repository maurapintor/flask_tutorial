from flask import render_template, redirect, url_for, abort

from app import app
from app.models import Comment, User
from flask_login import current_user, login_required, logout_user


@app.route('/clear')
@login_required
def clear():
    if current_user is not None:
        Comment.objects.delete()
        User.objects.delete()
        logout_user()
        return redirect(url_for('index'))
    else:
        abort(401, "User not logged in. Please login or register.")