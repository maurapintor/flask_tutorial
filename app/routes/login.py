from flask import render_template, redirect, url_for, abort
from werkzeug.security import check_password_hash
from app import app
from app.models import User
from app.forms import LoginForm
from flask_login import login_user

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.objects(username=form.username.data).first()
        if user is not None and len(user) > 1:
            if check_password_hash(user.password, form.password.data):
                login_user(user)
                return redirect(url_for('index'))
        else:
            abort(404, "User not found. Please register.")
    return render_template('login.html', title='Login', form=form)