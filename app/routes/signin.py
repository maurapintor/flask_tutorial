from flask import render_template, redirect, url_for, abort
from flask_login import login_user
from werkzeug.security import generate_password_hash

from app import app
from app.forms import SigninForm
from app.models import User


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = SigninForm()
    if form.validate_on_submit():
        same_email = User.objects(email=form.email.data)
        same_username = User.objects(username=form.username.data)
        if len(same_email) > 0:
            abort(403, "Forbidden. Email already in use.")
        if len(same_username) > 0:
            abort(403, "Forbidden. Username already in use.")
        else:
            hashpass = generate_password_hash(form.password.data, method='sha256')
            user = User(email=form.email.data,
                        username=form.username.data,
                        password=hashpass)
            user.save()
            login_user(user)

        return redirect(url_for('index'))
    return render_template('signin.html', title='Sign In', form=form)