from flask import render_template, redirect, url_for, abort

from app import app
from app.models import Comment
from app.forms import CommentForm
from flask_login import current_user, login_required


@app.route('/comment', methods=['GET', 'POST'])
@login_required
def comment():
    form = CommentForm()
    if form.validate_on_submit():
        if current_user is not None:
            c = Comment(user=current_user.username, text=form.text.data)
            c.save()
            return redirect(url_for('index'))
        else:
            # not necessary if we use login_required
            abort(401, "Unauthorized. Please register.")
    return render_template('comment.html', title='Comment', form=form)