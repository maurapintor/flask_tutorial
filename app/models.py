from flask_login import UserMixin

from app import db, login_manager


class User(db.Document, UserMixin):
    meta = {'collection': 'users'}
    email = db.StringField(max_length=30)
    username = db.StringField(max_length=30)
    password = db.StringField()


@login_manager.user_loader
def load_user(user_id):
    return User.objects(pk=user_id).first()


class Comment(db.Document):
    meta = {'collection': 'comments'}
    user = db.StringField(max_length=30)
    text = db.StringField(max_length=140)
